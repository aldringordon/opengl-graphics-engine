# OpenGL 3d Graphics Engine #

Final project from the U of S course CMPT-485/829 - Computer Graphics & Animation.

### 3D Animation of a City Scene ###

* Load multiple object files, and apply transformation matrices to each object independently
* Interpolation of camera keyframes to give animation between two camera locations
* Load textures for each object

### Required File Setup ###

* src/buildings/environment
* src/renderings
* src/camera.keyframes
* src/usaskville.campus

### Running Engine ###

```
use cmake to create the project
```
then:
```
./part6 usaskville.campus
```

Final mark for cmpt485: 96%
