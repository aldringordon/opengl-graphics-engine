// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <glfw3.h>
GLFWwindow* window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
using namespace glm;

#include <common/shader.hpp>
#include <common/controls.hpp>
#include <common/objloader.hpp>
#include <common/texture.hpp>
#include <common/vboindexer.hpp>

int width = 1024;
int height = 768;

void create_ppm(int frame, GLubyte* pixels) {
    
    glReadPixels(0, 0, 1024, 768, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    
    size_t cur;
    enum Constants { max_filename = 256 };
    char filename[max_filename];
    snprintf(filename, max_filename, "renderings/frame_%d.ppm", frame);
    
    FILE* file = fopen(filename, "w");
    fprintf(file, "P3\n%d %d\n%d\n", 1024, 768, 255);
    for (int i = 0; i < 768; i++) {
        for (int j = 0; j < 1024; j++) {
            cur = 4 * ((768 - i - 1) * 1024 + j);
            fprintf(file, "%3d %3d %3d ", pixels[cur], pixels[cur+1], pixels[cur + 2]);
        }
        fprintf(file, "\n");
    }
    fclose(file);
}

struct KeyFrame {
    // camera params for lookat
    // cx, cy, cz : center
    // dx, dy, dz : direction
    // rx, ry, rz : right
    // ux, uy ,uz : up
    // r : radius
    float cx, cy, cz, dx, dy, dz, rx, ry, rz, ux, uy, uz, r;
};

struct Camera {
    glm::vec3 center;
    glm::vec3 direction;
    glm::vec3 right;
    glm::vec3 up;
    float radius;
};

// https://blog.demofox.org/2015/08/08/cubic-hermite-interpolation/
// factorized representation of cubic hermite spline on an interval (p0, p1)
// int frames: number of frames
// float p0: position of first frame
// float p1: position of last frame
float cubicHermite(float a, float b, float c, float d, float t) {
    float a3 = -a/2.0 + (3.0 * b)/2.0 - (3.0 * c)/2.0 + d/2.0;
    float a2 = a - (5.0 * b)/2.0 + 2.0 * c - d/2.0;
    float a1 = -a/2.0 + c/2.0;
    float a0 = b;
    
    return a3*t*t*t + a2*t*t + a1*t + a0;
}

template <typename T>
inline T GetIndexClamped(const std::vector<T>& points, int index)
{
    if (index < 0)
        return points[0];
    else if (index >= int(points.size()))
        return points.back();
    else
        return points[index];
}

bool interpolate(std::vector<float> points, int numFrames, std::vector<float>& out_vector) {
    
    for (int i = 0; i < numFrames; i++) {
        float percent = ((float)i) / (float(numFrames - 1));
        float x = (points.size()-1) * percent;
        
        int index = int(x);
        float t = x - floor(x);
        float A = GetIndexClamped(points, index - 1);
        float B = GetIndexClamped(points, index + 0);
        float C = GetIndexClamped(points, index + 1);
        float D = GetIndexClamped(points, index + 2);
        
        float y = cubicHermite(A, B, C, D, t);
        out_vector.push_back(y);
    }
    
    return true;
}

// takes a vec3 vector
// creates 3 float vectors
// interpolates each vector
// merges the interpolated vectors into 1 vec3 vector
// i am not proud of this
bool interpolate3DVector(std::vector<glm::vec3> in_vector, int numFrames, std::vector<glm::vec3>& out_vector) {
    int size = (int)in_vector.size();
    
    std::vector<float> xs;
    std::vector<float> ys;
    std::vector<float> zs;
    xs.resize(size);
    ys.resize(size);
    zs.resize(size);
    
    for (int i = 0; i < size; i++) {
        xs[i] = in_vector[i].x;
        ys[i] = in_vector[i].y;
        zs[i] = in_vector[i].z;
    }
    
    std::vector<float> out_xs;
    std::vector<float> out_ys;
    std::vector<float> out_zs;
    
    interpolate(xs, numFrames, out_xs);
    interpolate(ys, numFrames, out_ys);
    interpolate(zs, numFrames, out_zs);
    
    int newSize = (int)out_xs.size();
    out_vector.resize(newSize);
    
    for (int i = 0; i < newSize; i++) {
        out_vector[i] = glm::vec3(out_xs[i], out_ys[i], out_zs[i]);
    }
    
    return true;
}

bool getLine(FILE* file, char* line) {
    char word[128];
    while( 1 ) {
        
        // grab line
        fgets(line, 1024, file);
        // grab first word in line
        int res = sscanf(line, "%s", word);
        if (res == EOF) {
            return false; // EOF found
        } else if ( strcmp( word, "#" ) == 0 ) {
            // found comment, ignore and read next line
            continue;
        } else {
            return true;
        }
    }
}

// takes in this keyframes file that i made up which is just a text file
// that includes 3*x number of 3vecs each representing a
// position, direction, up vector for the lookAt() function

// file was created by running the renderloop and positioning the camera
// to the desired position and direction, and then calling printViewMat
// on the ViewMatrix to save the matrix values for the camera
bool loadKeyFrames(const char* path,
                   std::vector<glm::vec3>& out_centers,
                   std::vector<glm::vec3>& out_directions,
                   std::vector<glm::vec3>& out_rights,
                   std::vector<glm::vec3>& out_ups,
                   std::vector<float>& out_radius) {
    printf("Loading KEYFRAMES file %s...\n", path);
    
    FILE* file = fopen(path, "r");
    if (file == NULL) {
        printf("Error opening %s\n", path);
        return false;
    }
    
    char line[1024];
    out_centers.clear();
    out_directions.clear();
    out_rights.clear();
    out_ups.clear();
    out_radius.clear();
    
    int numKeyFrames = 0;
    getLine(file, line);
    sscanf(line, "%d", &numKeyFrames);
    
    out_centers.resize(numKeyFrames);
    out_directions.resize(numKeyFrames);
    out_rights.resize(numKeyFrames);
    out_ups.resize(numKeyFrames);
    out_radius.resize(numKeyFrames);
    
    printf("Expecting: %d keyframes\n", numKeyFrames);
    
    for (int i = 0; i < numKeyFrames; i++){
        glm::vec3 center;
        glm::vec3 direction;
        glm::vec3 right;
        glm::vec3 up;
        float radius;
        float x, y, z;
        
        getLine(file, line); // should be blank
        
        getLine(file, line); // center
        sscanf(line, "%f %f %f", &x, &y, &z);
        center = glm::vec3(x, y, z);
        
        getLine(file, line); // direction
        sscanf(line, "%f %f %f", &x, &y, &z);
        direction = glm::vec3(x, y, z);
        
        getLine(file, line); // right
        sscanf(line, "%f %f %f", &x, &y, &z);
        right = glm::vec3(x, y, z);
        
        getLine(file, line); // up
        sscanf(line, "%f %f %f", &x, &y, &z);
        up = glm::vec3(x, y, z);
        
        getLine(file, line); // radius
        sscanf(line, "%f", &radius);
        
        out_centers[i] = center;
        out_directions[i] = direction;
        out_rights[i] = right;
        out_ups[i] = up;
        out_radius[i] = radius;
    }
    
    return true;
}

// used to capture the position of the viewmatrix when choosing camera
// positions to use as keyframes for the animation
void printViewMat(glm::mat4 matrix) {
    printf("\n");
    printf("printMatrix():\n");
    printf("%f %f %f position\n", matrix[0][0], matrix[0][1], matrix[0][2]);
    printf("%f %f %f direction\n", matrix[1][0], matrix[1][1], matrix[1][2]);
    printf("%f %f %f up\n", matrix[2][0], matrix[2][1], matrix[2][2]);
    printf("\n");
}

void printVec(glm::vec3 vec) {
    printf("%f %f %f\n",
           vec.x, vec.y, vec.z);
}

int main(int argc, char* argv[])
{

    /**********************************/
    /*** APPLICATION INITIALIZATION ***/
    /**********************************/
    
    // Initialise GLFW
    if( !glfwInit() )
    {
        fprintf( stderr, "Failed to initialize GLFW\n" );
        return -1;
    }
    
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    
    // Open a window and create its OpenGL context
    window = glfwCreateWindow( 1024, 768, "CMPT 485", NULL, NULL);
    if( window == NULL ){
        fprintf( stderr, "Failed to open GLFW window.\n" );
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        return -1;
    }
    
    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024/2, 768/2);
    
    /**********************************/
    /***** OPENGL INITIALIZATION ******/
    /**********************************/
    
    
    // Dark blue background
    glClearColor(0.8f, 0.8f, 0.8f, 0.0f);
    
    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);
    
    
    
    // Create and compile our GLSL program from the shaders
    GLuint programID = LoadShaders( "TransformVertexShader.vertexshader", "ColorFragmentShader.fragmentshader" );
    // Use our shader
    glUseProgram(programID);
    
    // Get a handle for our "MVP" uniform
    GLuint ViewProjectionMatrixID = glGetUniformLocation(programID, "VP");
    GLuint ModelMatrixID = glGetUniformLocation(programID, "M");
    
    // Initialize GLFW control callbacks
    initializeMouseCallbacks();
    
    // Projection and Model matrices are fixed
    glm::mat4 ProjectionMatrix = getProjectionMatrix();
    
    /**************************************/
    /**** LOAD BUILDINGS INFORMATION ******/
    /**************************************/
    
    std::vector<Building> buildings;
    std::vector<Model> models;
    std::vector<std::string> modelNames;
    char campus_name[1024];
    char fileName[1024];
    std::string pathToModels = "buildings";

    if (argc < 2) {
        printf("No .campus file passed. Try ./part6 filename\n");
        printf("Using \"test.campus\"\n");
        loadBuildings("test.campus", campus_name, buildings);
    }
    else {
        loadBuildings(argv[1], campus_name, buildings);
    }
    
    for (int i  = 0; i < buildings.size(); i++) {

        printf("\n");
        printf("Found building %s\n", buildings[i].modelsFilename.c_str());
        printf("Txyz = %f, %f, %f\n", buildings[i].tx, buildings[i].ty, buildings[i].tz);
        printf("Sxyz = %f, %f, %f\n", buildings[i].sx, buildings[i].sy, buildings[i].sz);
        printf("Rxyz = %f, %f, %f\n", buildings[i].rx, buildings[i].ry, buildings[i].rz);
        printf("\n");
        
        std::vector<Model> tmp;
        sprintf(fileName, "%s/%s/%s.models", pathToModels.c_str(), buildings[i].modelsFilename.c_str(), buildings[i].modelsFilename.c_str());
        loadModels(fileName, tmp);
        
        std::vector<Model>::iterator tmpIter;

        for(tmpIter = tmp.begin();
            tmpIter != tmp.end();
            tmpIter++)
        {
            tmpIter->sx = buildings[i].sx;
            tmpIter->sy = buildings[i].sy;
            tmpIter->sz = buildings[i].sz;
            
            tmpIter->rx = buildings[i].rx;
            tmpIter->ry = buildings[i].ry;
            tmpIter->rz = buildings[i].rz;
            tmpIter->ra = buildings[i].ra;
            
            tmpIter->tx = buildings[i].tx;
            tmpIter->ty = buildings[i].ty;
            tmpIter->tz = buildings[i].tz;
            
            models.push_back(*tmpIter);
            modelNames.push_back(buildings[i].modelsFilename);
        }
    }
    
    // Get a handle for our "myTextureSampler" uniform
    GLuint TextureID  = glGetUniformLocation(programID, "myTextureSampler");
    
    int numModels = models.size();
    
    GLuint* vaoIDs = new GLuint[numModels];
    GLsizei* numVertices = new GLsizei[numModels];
    GLuint* textures = new GLuint[numModels];

    std::vector<glm::mat4> ModelMatrix;
    ModelMatrix.resize(numModels);
    
    glGenVertexArrays(numModels, vaoIDs);
    
    for (int i  = 0; i < numModels; i++) {
        
        printf("\t\t%s\n", models[i].textureFilename.c_str());
        
        // Read our .obj file
        std::vector<glm::vec3> vertices;
        std::vector<glm::vec2> uvs;
        std::vector<glm::vec3> normals;
        
        sprintf(fileName, "%s/%s/%s", pathToModels.c_str(), modelNames[i].c_str(), models[i].objFilename.c_str());
        bool loadSuccess = loadOBJ(fileName, models[i].materialTag.c_str(), vertices, uvs, normals);
        if (!loadSuccess) {
            return -1;
        }
        
        // for PNG images, flip t texture coordinate because image is upside down
        for (int i = 0; i < uvs.size(); i++) {
            uvs[i][1] = -uvs[i][1];
        }
        
        // keep track of vbo sizes for drawing later
        numVertices[i] = vertices.size();
        
        // set model matrix from info in .models file
        glm::mat4 I = glm::mat4(1.0f);
        glm::mat4 Ms = glm::scale(I, glm::vec3(models[i].sx, models[i].sy, models[i].sz));
        glm::float_t angle_radians = models[i].ra*3.1416/180;
        glm::mat4 Mr = glm::rotate(I, angle_radians,glm::vec3(models[i].rx, models[i].ry, models[i].rz));
        glm::mat4 Mt = glm::translate(I, glm::vec3(models[i].tx, models[i].ty, models[i].tz));
        ModelMatrix[i] = Mt * Mr * Ms;

        
        // printf("ModelMat%d\n", i);
        // printMat(ModelMatrix[i]);
        
        // Load vertices into a VBO
        GLuint vertexbuffer;
        glGenBuffers(1, &vertexbuffer);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glBufferData(GL_ARRAY_BUFFER, numVertices[i] * sizeof(glm::vec3), &vertices[0], GL_STATIC_DRAW);
        
        // Load normals into a VBO
        GLuint normalsbuffer;
        glGenBuffers(1, &normalsbuffer);
        glBindBuffer(GL_ARRAY_BUFFER, normalsbuffer);
        glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0], GL_STATIC_DRAW);
        
        // Load texture coords into a VBO
        GLuint uvbuffer;
        glGenBuffers(1, &uvbuffer);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);
        
        // bind VAO in order to save attribute state
        glBindVertexArray(vaoIDs[i]);
        
        if (strcmp(models[i].textureFilename.c_str(), "none")!=0) { // if textureFilename set to "none", ignore
            sprintf(fileName, "%s/%s/%s", pathToModels.c_str(), modelNames[i].c_str(), models[i].textureFilename.c_str());
            GLuint t;
            if (strstr(models[i].textureFilename.c_str(), ".png")!=NULL){
                t = loadPNG(fileName);
            }
            else {
                t = loadBMP_custom(fileName);
            }
            textures[i] = t;
        
            // Set our "myTextureSampler" sampler to user Texture Unit i
            glUniform1i(TextureID, i);
        }
    
        // 1st attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                              0,                  // attribute. No particular reason for 0, but must match the layout in the shader.
                              3,                  // size
                              GL_FLOAT,           // type
                              GL_FALSE,           // normalized?
                              0,                  // stride
                              (void*)0            // array buffer offset
                              );
        
        // 2nd attribute buffer : normals
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, normalsbuffer);
        glVertexAttribPointer(
                              1,                  // attribute. No particular reason for 1, but must match the layout in the shader.
                              3,                  // size
                              GL_FLOAT,           // type
                              GL_FALSE,           // normalized?
                              0,                  // stride
                              (void*)0            // array buffer offset
                              );
        
        // 3rd attribute buffer : uvs
        glEnableVertexAttribArray(2);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(
                              2,                  // attribute. No particular reason for 2, but must match the layout in the shader.
                              2,                  // size
                              GL_FLOAT,           // type
                              GL_FALSE,           // normalized?
                              0,                  // stride
                              (void*)0            // array buffer offset
                              );
        
        
        
        /**********************************/
        /*** UNBIND VERTEX-ARRAY OBJECT ***/
        /**********************************/
        glBindVertexArray(0);
    }
        
    // ANIMATION LOOP probably the most
    // inefficient method you'll ever see
    
    std::vector<glm::vec3> centers;
    std::vector<glm::vec3> directions;
    std::vector<glm::vec3> rights;
    std::vector<glm::vec3> ups;
    std::vector<float> radius;
    
    loadKeyFrames("render.keyframes",
                  centers,
                  directions,
                  rights,
                  ups,
                  radius);
    
    printf("read: %i keyframes\n", (int)centers.size());
    
    int numKeyFrames = (int)centers.size();
    int fps = 30; // framerate
    
    int totalFrames = numKeyFrames * fps;
    
    printf("\nKeyframe positions:\n");
    for (int i = 0; i < numKeyFrames; i++) {
        printVec(centers[i]);
    }
    
    std::vector<glm::vec3> allCenters;
    std::vector<glm::vec3> allDirections;
    std::vector<glm::vec3> allRights;
    std::vector<glm::vec3> allUps;
    std::vector<float> allRadius;
    
    interpolate3DVector(centers, totalFrames, allCenters);
    interpolate3DVector(directions, totalFrames, allDirections);
    interpolate3DVector(rights, totalFrames, allRights);
    interpolate3DVector(ups, totalFrames, allUps);
    interpolate(radius, totalFrames, allRadius);
    
    for (int i = 0; i < allCenters.size(); i++) {
        printVec(allCenters[i]);
    }
    
    printf("total frames: %i\n", (int)allCenters.size());
    printf("also total frames: %i\n", totalFrames);
    
    // RENDER
    
    printf("\n  rendering . . . \n");
    
    GLubyte* pixels = (GLubyte*)malloc(4 * 1024 * 768);
    glReadBuffer(GL_BACK);
    glClearColor(0.0, 0.0, 0.0, 0.0);
    glPixelStorei(GL_PACK_ALIGNMENT, 1);
    glViewport(0, 0, 1024, 768);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    
    for (int i = 0; i < totalFrames; i++) {
        printf("rendering frame: %i/%i . . . ", i+1, totalFrames);
        
        updateFrame(allCenters[i],
                    allDirections[i],
                    allRights[i],
                    allUps[i],
                    allRadius[i]);
        glm::mat4 View = getViewMatrix();
        glm::mat4 ViewProj = ProjectionMatrix * View;
        
        glUniformMatrix4fv(ViewProjectionMatrixID, 1, GL_FALSE, &ViewProj[0][0]);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        for (int i = 0; i < numModels; i++) {
            
            // bind VAO to recall VBOs state
            glBindVertexArray(vaoIDs[i]);
            glActiveTexture(i);
            glBindTexture(GL_TEXTURE_2D, textures[i]);
            // Set our "myTextureSampler" sampler to user Texture Unit 0
            glUniform1i(TextureID, 0);
            
            // Set our Model transform matrix
            glm::mat4 M = ModelMatrix[i];
            glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &M[0][0]);
            
            glDrawArrays(GL_TRIANGLES, 0, numVertices[i] );
            
            // Unbind VAO
            glBindVertexArray(0);
        }
        
        glfwSwapBuffers(window);
        glfwPollEvents();
        
        create_ppm(i, pixels);
        
        printf("DONE\n");
    }
    
    free(pixels);
    printf(". . . finished rendering\n");
    
    do{

        // Get updated view matrix (mouse input handled with callback functions)
        glm::mat4 ViewMatrix = getViewMatrix();
        glm::mat4 VP = ProjectionMatrix * ViewMatrix;
        
        // Send our transformation to the currently bound shader,
        // in the "MVP" uniform
        glUniformMatrix4fv(ViewProjectionMatrixID, 1, GL_FALSE, &VP[0][0]);
        
        // Get information about camera position
        if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS) {
            // printViewMat(ViewMatrix);
            printCamera();
        }
        
        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        
        /**********************************/
        /************** DRAW  *************/
        /**********************************/
        
        for (int i = 0; i < numModels; i++) {
            
            // bind VAO to recall VBOs state
            glBindVertexArray(vaoIDs[i]);
            glActiveTexture(i);
            glBindTexture(GL_TEXTURE_2D, textures[i]);
            // Set our "myTextureSampler" sampler to user Texture Unit 0
            glUniform1i(TextureID, 0);
            
            // Set our Model transform matrix
            glm::mat4 M = ModelMatrix[i];
            glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &M[0][0]);
            
            glDrawArrays(GL_TRIANGLES, 0, numVertices[i] );
            
            // Unbind VAO
            glBindVertexArray(0);
        }
        
        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
        
    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
          glfwWindowShouldClose(window) == 0 );
    
    // Cleanup VBO and shader
    glDeleteProgram(programID);
    for (int i = 0; i < numModels; i++) {
        glDeleteVertexArrays(1, &vaoIDs[i]);
    }
    
    // Close OpenGL window and terminate GLFW
    glfwTerminate();
    
    return 0;
}

